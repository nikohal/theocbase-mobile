#include "school_detail.h"

school_detail::school_detail()
{
    mTheme = "";
}

void school_detail::load(int id)
{
    school s;
}

QString school_detail::theme()
{
    return mTheme;
}

void school_detail::setTheme(QString theme)
{
    mTheme = theme;
}

QString school_detail::source()
{
    return mSource;
}

void school_detail::setSource(QString source)
{
    mSource = source;
}

QString school_detail::studentName()
{
    return mStudentName;
}

void school_detail::setStudentName(QString name)
{
    mStudentName = name;
    emit studentnameChanged();
}

QVariant school_detail::studies()
{
    QStringList studies;

    school s;
    QList<schoolStudy*> slist = s.getStudies();
    foreach (schoolStudy *study, slist) {
        studies.append(QVariant(study->getNumber()).toString() + " " + study->getName());
        qDebug() << study->getNumber() << study->getName();
    }

    //QVariant::fromValue(mSchoolList));
    return QVariant::fromValue(studies);
}
