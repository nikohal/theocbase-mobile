import QtQuick 2.0
import net.theocbase.mobile 1.0

Rectangle {
    id: publisherspage
    width: 200
    height: 500

    Publishers {
        id: _publishers
    }

    Component.onCompleted: {
        publishersListView.model = _publishers.getAllPublishersList()
    }

    ListView {
        id: publishersListView
        anchors.fill: parent
        height: parent.height
        orientation: ListView.Vertical
        model: publishersList
        spacing: 0
        delegate: listComponent
    }

    // list view component
    Component {
        id: listComponent
        Rectangle {
            height: 50
            width: parent.width

            border.width: 0
            border.color: "gray"

            //property int idNo: idNumber
            Column{
                anchors.fill: parent
                anchors.margins: 5
                //width: parent.width
                Text {
                    id: textItem
                    text: model.modelData.name
                    font.bold: true
                }
            }
            Rectangle {
                width: parent.width
                anchors.bottom: parent.top
                anchors.topMargin: 0
                height: 1
                color: "gray"
                visible: index == 0
            }
            Rectangle {
                width: parent.width
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 0
                height: 1
                color: "gray"
            }
        }
    }

    Component {
        id: listpage
        Details {}
    }
}
