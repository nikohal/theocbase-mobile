#include "messagebox.h"
#include <QDebug>
#include <QMessageBox>

MessageBox::MessageBox()
{
}

void MessageBox::show(QString title, QString message){
    QMessageBox::information(0,title,message);
    emit buttonClicked(false);
}

void MessageBox::showOKCancel(QString title, QString message){
    if (QMessageBox::question(0,title,message,QMessageBox::Yes,QMessageBox::No) == QMessageBox::Yes) {
        emit buttonClicked(true);
    }else{
        emit buttonClicked(false);
    }
}

