import QtQuick 2.0
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.1
import QtQuick.Dialogs 1.2
import net.theocbase.mobile 1.0

Rectangle {
    id: settingsPage
    width: 200
    height: 500

    color: "#dfe8df"

    function logged(ok){
        buttonLogin.visible = !ok;
        buttonLogout.visible = ok;
        buttonSync.visible = ok;
    }

    Connections {
        target: ccloud
        onLoggedChanged: {
            logged(ok);
        }
    }

    Component.onCompleted: {
        logged(ccloud.logged());
    }

    SynchronizePage {
        id: syncpage
        z: 2
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
    }

    MsgBox {
        id: msg
    }

    Connections {
        target: msg
        onButtonClicked: {
            console.log(ok);
            if (ok){
                syncpage.runSync();
            }
        }
    }

    Button {
        id: buttonLogin
        anchors.horizontalCenter: parent.horizontalCenter
        y: 100
        text: qsTr("Login")
        onClicked: {
            stackView.push(Qt.resolvedUrl("LoginPage.qml"));
        }
    }

    Button {
        id: buttonLogout
        anchors.horizontalCenter: parent.horizontalCenter
        y: 160
        text: qsTr("Logout")
        onClicked: {
            ccloud.logout();
        }
    }

    Button {
        id: buttonSync
        anchors.horizontalCenter: parent.horizontalCenter
        y: 200
        anchors.centerIn: parent.Center
        text: qsTr("Check updates")
        onClicked: {
            if (ccloud.checkCloudUpdates()){
                msg.showOKCancel("TheocBase Cloud","Updates available. Do you want synchronize?");
            }else{
                msg.show("TheocBase Cloud", "No updates found!");
            }
        }
    }

    Switch {
        anchors.horizontalCenterOffset: 0
        anchors.top: parent.top
        anchors.topMargin: 50
        anchors.horizontalCenter: parent.horizontalCenter
    }
}
