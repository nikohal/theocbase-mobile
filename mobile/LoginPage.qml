import QtQuick 2.0
import QtQuick.Controls 1.1

Rectangle {
    width: 400
    height: 500
    color: "#2c57f2"

    Component.onCompleted: {
        console.log("login completed")
        console.log(ccloud.logged())
    }

    TextField {
        id: textUsername
        anchors.horizontalCenter: parent.horizontalCenter
        y: 165
        anchors.horizontalCenterOffset: 0
        placeholderText: qsTr("Username")
    }

    TextField {
        anchors.horizontalCenter: parent.horizontalCenter
        id: textPassword
        y: 193
        anchors.horizontalCenterOffset: 0
        echoMode: TextInput.Password
        placeholderText: qsTr("Password")
    }

    Button {
        anchors.horizontalCenter: parent.horizontalCenter
        id: button1
        x: 162
        y: 244
        width: 100
        text: qsTr("Login")
        anchors.horizontalCenterOffset: 0
        onClicked: {
            if (!ccloud.login(textUsername.text,textPassword.text)){
                labelError.visible = true
                labelError.text = "Login Failed"
                return
            }else{
                labelError.visible = false
                // hide keyboard on iOS
                Qt.inputMethod.hide()
                //settingsPage.logged(true)
                stackView.pop()
            }
        }
    }

    Label {
        id: labelError
        anchors.horizontalCenter: parent.horizontalCenter
        y: 222
        text: "error"
        anchors.horizontalCenterOffset: 0
        visible: false
        color: "red"
    }


}
