#include <UIKit/UIKit.h>
#include <QtGui>
#include <QtQuick>
#include "messagebox.h"
#include <QDebug>

@interface myViewController : UIViewController<UIAlertViewDelegate>
{
    MessageBox *m_Msg;
}
@end

@implementation myViewController

- (id)init
{
    self = [super init];
    return self;
}

- (id) initWithMessageBox:(MessageBox *)msg
{
    self = [super init];
    if (self) {
        m_Msg = msg;
    }
    return self;
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    Q_UNUSED(alertView);
    if (buttonIndex == 0)
    {
        qDebug() << "Cancel clicked";
        emit m_Msg->buttonClicked(false);
    }
    else if(buttonIndex == 1)
    {
        qDebug() << "OK clicked";
        emit m_Msg->buttonClicked(true);
    }
}

@end

MessageBox::MessageBox()
{
}

void MessageBox::show(QString title, QString message){
    myViewController *mvc = [[myViewController alloc ] initWithMessageBox:this];
    UIAlertView * alert = [[UIAlertView alloc ] initWithTitle:title.toNSString()
        message:message.toNSString()
        delegate:mvc
        cancelButtonTitle:@"OK"
        otherButtonTitles: nil];
    [alert show];
    [alert release];
}

void MessageBox::showOKCancel(QString title, QString message){
    myViewController *mvc = [[myViewController alloc ] initWithMessageBox:this];

    UIAlertView * alert = [[UIAlertView alloc ] initWithTitle:title.toNSString()
        message:message.toNSString()
        delegate:mvc
        cancelButtonTitle:@"Cancel"
        otherButtonTitles: nil];
    [alert addButtonWithTitle:@"OK"];
    [alert show];
    [alert release];
}
