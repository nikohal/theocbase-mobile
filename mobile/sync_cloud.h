#ifndef SYNC_CLOUD_H
#define SYNC_CLOUD_H

#include <QThread>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QUrl>
#include <QEventLoop>
#include <QDebug>
#include <QString>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonValue>
#include <QJsonObject>
#include <QUuid>
#include <QSettings>
#include "../sql_class.h"

class sync_cloud : public QThread
{
    Q_OBJECT
public:
    sync_cloud();
    void sync();
    bool cloudUpdateAvailable();

private:
    sql_class *sql;
    QHash<QString,sql_item> getLocalChangeset();
    bool getRemoteChangeSets();
    QJsonArray getChangesFromCloud();
    int pushChangesToCloud(QJsonDocument doc);
    void saveChangesToDatabase(QHash<QString, sql_item>changeset);

    int m_userid;
    int m_changeid;
    QString idMapping(QString fromTable, QString uuid);
    QHash<QString,sql_item> mRemoteChangeSet1;
    QHash<QString,sql_item> mRemoteChangeSet2;

};

#endif // SYNC_CLOUD_H
