import QtQuick 2.1
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.1
import QtQuick.Controls.Styles 1.1
import net.theocbase.mobile 1.0

Item {
    id: mainpage

    School {
        id: _school
    }

    Connections {
        target: _school
        onListUpdated: {
            console.log("list updated signal")
            schoolListView.model = list
            datetext.text = Qt.formatDate(_school.currentDate(),Qt.DefaultLocaleShortDate)
        }
    }

    Component.onCompleted: {
        console.log("init list")
        _school.initList()        
    }

    Rectangle {
        id: blueRect
        width: parent.width
        height: parent.height
        implicitHeight: 600
        implicitWidth: 400
        //anchors.top: 80

        ToolBar {
            height: 45
            id: toolbar
            z: 1
            RowLayout {
                id: rowLayout1
                anchors.fill: parent
                ToolButton {
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    text: "< Prev"
                    iconSource: "qrc:///icons/appbar.navigate.previous.png"
                    //onClicked: stackView.push(Qt.resolvedUrl("MainPage.qml"))
                    onClicked: {
                        _school.prevWeek()
                    }

                    style: ToolButtonStyle{}
                }
                ToolButton {
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    iconSource: "qrc:///icons/appbar.calendar.day.png"
                    text: "Today"
                    style: ToolButtonStyle{}
                    onClicked: {
                        _school.initList()
                    }
                }
                ToolButton {
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                    text: "Next >"
                    iconSource: "qrc:///icons/appbar.navigate.next.png"
                    //onClicked: stackView.push(Qt.resolvedUrl("Page2.qml"))
                    onClicked: {
                         _school.nextWeek()
                    }

                    style: ToolButtonStyle{}
                }

            }
            style: ToolBarStyle {
                padding {
                    left: 8
                    right: 8
                    top: 3
                    bottom: 3
                }
                background: Rectangle {
                    implicitWidth: 100
                    implicitHeight: 40
                    border.width: 0
                    color: "#e7e7e7"
                    Rectangle {
                        anchors.bottom: parent.bottom
                        height: 1
                        anchors.left: parent.left
                        anchors.right: parent.right
                        color: "grey"
                    }
                }
            }
        }

        Text {
            id: datetext
            anchors.top: toolbar.bottom
            anchors.horizontalCenter: parent.horizontalCenter
            text: "Current date"
            anchors.horizontalCenterOffset: 0
            anchors.topMargin: 10
        }

        ListView {
            id: schoolListView
            //anchors.fill: parent
            anchors.topMargin: toolbar.height + datetext.height + 10
            anchors.bottom: parent.bottom
            width: parent.width

            height: parent.height - toolbar.height - datetext.height - 20
            orientation: ListView.Vertical
            spacing: 0
            delegate: listComponent           
        }

        // list view component
        Component {
            id: listComponent
            Rectangle {
                id: listRect
                height: 60
                width: parent.width

                border.width: 0
                border.color: "grey"

                property int idNo: model.modelData.idNumber // idNumber

                Row {
                    anchors.fill: parent
                    anchors.margins: 5
                    spacing: 5
                    width: parent.width - 10
                    height: parent.height -10

                    Column {
                        width: 50
                        Rectangle {
                            id: namebox
                            color: "#116bc6"
                            height: 50
                            width: 50

                            Text {
                                anchors.fill: parent
                                anchors.margins: 5
                                color: "white"
                                width: parent.width
                                elide: Text.ElideRight
                                text: model.modelData.numbername
                            }
                        }
                    }

                    Column{
                        width: parent.width - 50

                        spacing: 10
                        Text {
                            id: textItem
                            text: model.modelData.name
                            //text: name
                            font.bold: true
                            elide: Text.ElideRight
                            //wrapMode: Text.WordWrap
                            width: parent.width
                        }
                        Text {
                            id: number
                            text: model.modelData.value
                            //text: value
                        }
                    }
                }

                Rectangle {
                    width: parent.width
                    anchors.bottom: parent.top
                    anchors.topMargin: 0
                    height: 0.5
                    color: "gray"
                    visible: index == 0
                }
                Rectangle {
                    width: parent.width
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: 0
                    height: 0.5
                    color: "gray"
                }

                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        console.debug(idNo + " list index:" + index)
                        //detailPage.detobj = _school.getAssignment(index)
                        var tobj = _school.getAssignment(index)
                        toolbar.height = 0
                        navbar.height = 0
                        //listpage.loadMe(idNumber)
                        stackView.push({item: detailPage, properties: {detailid: idNo, detobj: _school.getAssignment(index)}})
                        //stackView.push({item: Qt.resolvedUrl("Details.qml"), properties: {detailid: idNumber}})
                    }
                }
            }
        }

        Component {
            id: detailPage
            Details {}
        }

        ListModel {
            id: myModelTest
            ListElement { name: "Yksi"; value: "11111"; idNumber: 2 }
            ListElement { name: "Kaksi"; value: "22222"; idNumber: 3 }
        }

    }
}
