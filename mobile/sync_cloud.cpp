#include "sync_cloud.h"

sync_cloud::sync_cloud()
{
    sql = &Singleton<sql_class>::Instance();
    QSettings settings;
    m_userid = settings.value("cloud/id",-1).toInt();
}

void sync_cloud::sync()
{
    if (m_userid <= 0){
        qDebug() << "not logged in";
        return;
    }
    qDebug() << "start";

    // reset values
    //sql->saveSetting("last_sync_id","0");
    //sql->saveSetting("last_sync_time","-1");

    // Get local changeset
    QHash<QString, sql_item>localchangeset = getLocalChangeset();
    qDebug() << "localchangeset" << localchangeset.count();

    // Get remote changeset(s)
    if (!getRemoteChangeSets()){
        qDebug() << "error when getting remote change sets";
        return;
    }
    qDebug() << "remotechangeset1" << mRemoteChangeSet1.count() << "remotechangeset2" << mRemoteChangeSet2.count();

    // Compare changesets
    QHash<QString, sql_item>duplicatevalues;
    QHashIterator<QString, sql_item> i(localchangeset);
    while (i.hasNext()) {
        i.next();

        if (mRemoteChangeSet1.contains(i.key()) || mRemoteChangeSet2.contains(i.key())){
            duplicatevalues.insert(i.key(),i.value());
        }
    }

    if (localchangeset.count() == 0 && mRemoteChangeSet1.count() == 0 && mRemoteChangeSet2.count() == 0){
        qDebug() << "Nothing to sync";
        return;
    }

    // Resolve conflicts
    if (duplicatevalues.count() > 0){
        QMessageBox::information(0,"","Syncronization conflict. Should be resolved...." +
                                 QVariant(duplicatevalues.count()).toString());
        // TODO: Resolve conflict
        //return;
    }

    // Push changes


    if (localchangeset.count() > 0){
        //QJsonArray all_array;

        QHash<QString,QJsonArray>tablelist;

        QJsonArray tablearray;

        foreach(sql_item item,localchangeset){
            QString tablename = "";
            QJsonObject fobjs;
            QHashIterator<QString, QVariant> i(item);
            while (i.hasNext()){
                i.next();
                if (i.key() == "id" || i.key() == "time_stamp") continue;
                if (i.key() == "tablename"){
                    tablename = i.value().toString();
                    continue;
                }
                fobjs[i.key()] = i.value().toString();
            }

            if (tablelist.contains(tablename)){
                tablearray = tablelist.value(tablename);
            }else{
                tablearray = QJsonArray();
            }

            //qDebug() << "table" << tablename << "json object" << fobjs;

            tablearray.append(fobjs);
            tablelist.insert(tablename,tablearray);
        }

        qDebug() << "tablelist.count" << tablelist.count();

        QJsonArray rootArray;
        QHashIterator<QString,QJsonArray> i2(tablelist);
        while (i2.hasNext()){
            i2.next();
            QJsonObject tableObject;
            tableObject["table"] = i2.key();
            tableObject["rows"] = i2.value();

            //qDebug() << "\n\n" << i2.value() << "\n" << i2.key();

            rootArray.append(tableObject);
        }
        QJsonDocument doc(rootArray);

        qDebug() << "Localchangeset is ready to send";// << doc;

        int newchangeid = pushChangesToCloud(doc);
        qDebug() << "newchangeid" << newchangeid;
        m_changeid = newchangeid;
    }


    // Save changes to local database
    if (mRemoteChangeSet1.count() > 0 || mRemoteChangeSet2.count() > 0){
        saveChangesToDatabase(mRemoteChangeSet1);
        saveChangesToDatabase(mRemoteChangeSet2);
    }else{
        qDebug() << "No changes from cloud";
    }

    // Save sync id
    sql->saveSetting("last_sync_id",QVariant(m_changeid).toString());
    // Save sync time
    QDateTime date = QDateTime::currentDateTime();
    sql->saveSetting("last_sync_time",QVariant(date.toTime_t()).toString());
}

bool sync_cloud::cloudUpdateAvailable()
{
    //https://lab.theocbase.net/api.php?history&user_id=8

    QString urlstring = "https://lab.theocbase.net/api.php?history&user_id=" + QVariant(m_userid).toString();

    QNetworkAccessManager *m_manager = new QNetworkAccessManager();
    QNetworkReply *reply = m_manager->get( QNetworkRequest(QUrl( urlstring )));

    QEventLoop loop;
    connect(reply,SIGNAL(finished()),&loop,SLOT(quit()));
    loop.exec();

    QVariant statusCode = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute);
    if (statusCode.toInt() != 200){
        qDebug() << "Server error response code" << statusCode;
    }

    QByteArray b = reply->readAll();
    QJsonDocument d = QJsonDocument::fromJson(b);

    int lastsyncid = sql->getSetting("last_sync_id","0").toInt();
    qDebug() << "last sync id" << lastsyncid;
    int newsyncid = 0;

    if (d.isArray()){
        QJsonArray a = d.array();
        foreach(QJsonValue v, a){
            QJsonObject o = v.toObject();
            QString idstr = o.value("id").toString();
            newsyncid = idstr.toInt();
        }
    }

    return (newsyncid > lastsyncid);
}

QJsonArray sync_cloud::getChangesFromCloud()
{
    qDebug() << "sync_cloud";
    QNetworkAccessManager *m_manager = new QNetworkAccessManager();
    QString last_sync_id = sql->getSetting("last_sync_id","0");
    QString urlstring = "https://lab.theocbase.net/api.php?history_chg2&user_id=" +
            QVariant(m_userid).toString() +
            "&change_id=" + last_sync_id;
    QNetworkReply *reply = m_manager->get(
                QNetworkRequest( QUrl( urlstring )));

    QEventLoop loop;
    qDebug() << "sync_cloud3";
    connect(reply,SIGNAL(finished()),&loop,SLOT(quit()));
    loop.exec();

    QVariant statusCode = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute);
    if (statusCode.toInt() != 200){
        qDebug() << "Server error response code" << statusCode;
    }else{
        qDebug() << "Server response code ok" << statusCode;
    }

    QByteArray b = reply->readAll();
    QJsonDocument d = QJsonDocument::fromJson(b);
    QJsonArray return_array;
    //qDebug() << d;
    if (d.isArray()){

        return_array = d.array();
    }else{
        qDebug() << "Invalid JSON message!";
    }

    return return_array;
}

int sync_cloud::pushChangesToCloud(QJsonDocument doc)
{
    qDebug() << "sync_cloud_post";
    QNetworkAccessManager *m_manager = new QNetworkAccessManager();
    QString urlstring = "https://lab.theocbase.net/api.php";

    QByteArray postData;
    postData.append("datachange2=datachange&");
    postData.append("user_id=" + QVariant(m_userid).toString() + "&");

#if defined(Q_OS_IOS) || defined(Q_OS_ANDROID) || defined(Q_OS_WINRT)
    postData.append("from=mobile&");
#else
    postData.append("from=desktop&");
#endif

    postData.append("json=" + doc.toJson(QJsonDocument::Compact));

    QNetworkRequest request;
    request.setUrl(QUrl(urlstring));
    //request.setHeader(QNetworkRequest::ContentTypeHeader,"text/plain");

    QNetworkReply *reply = m_manager->post(request,postData);

    QEventLoop loop;
    qDebug() << "sync_cloud3";
    connect(reply,SIGNAL(finished()),&loop,SLOT(quit()));
    loop.exec();

    QVariant statusCode = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute);
    if (statusCode.toInt() != 200) {
        qWarning() << "error response code " << statusCode.toInt();
    }
    QByteArray b = reply->readAll();
    QString returnvalue = b;
    //this->exec();
    qDebug() << "reply : " << returnvalue;
    QJsonDocument d = QJsonDocument::fromJson(b);   
    QString change_id_str = d.array().first().toObject().value("change_id").toString();
    return QVariant(change_id_str).toInt();
}

QHash<QString, sql_item> sync_cloud::getLocalChangeset()
{
    QHash<QString, sql_item>localchangeset;

    QString timestamp = sql->getSetting("last_sync_time","-1");

    QList<QPair<QString,QString> > tables;

    // tables and SQL clauses to query changes
    tables.append(qMakePair(QString("persons"),QString("SELECT * FROM persons")));
    tables.append(qMakePair(QString("school_schedule"),QString("SELECT * FROM school_schedule")));
    tables.append(qMakePair(QString("school"),
                            QString(
                                "SELECT classnumber,"
                                "ifnull((SELECT uuid FROM school_schedule WHERE id = school_schedule_id),-1) AS school_schedule_id,"
                                "ifnull((SELECT uuid FROM persons WHERE id = student_id),-1) AS student_id,"
                                "ifnull((SELECT uuid FROM persons WHERE id = assistant_id),-1) AS assistant_id,"
                                "ifnull((SELECT uuid FROM persons WHERE id = volunteer_id),-1) AS volunteer_id,"
                                "date,completed,note,timing,setting_id,active,uuid FROM school"
                                )));
    tables.append(qMakePair(QString("studies"),QString("SELECT * FROM studies")));
    tables.append(qMakePair(QString("school_break"),QString("SELECT * FROM school_break")));
    tables.append(qMakePair(QString("school_settings"),QString("SELECT * FROM school_settings")));
    tables.append(qMakePair(QString("exceptions"),QString("SELECT * FROM exceptions")));
    tables.append(qMakePair(QString("student_studies"),
                            QString(
                                "SELECT study_number, start_date, end_date, exercises, active, uuid,"
                                "ifnull((SELECT uuid FROM persons WHERE id = student_id),-1) AS student_id FROM student_studies"
                                )));
    tables.append(qMakePair(QString("unavailables"),
                            QString(
                                "SELECT start_date, end_date, active, uuid,"
                                "ifnull((SELECT uuid FROM persons WHERE id = person_id),-1) AS person_id FROM unavailables"
                                )));

    QPair<QString,QString> tableinfo;
    foreach(tableinfo, tables){
        sql_items allp = sql->selectSql(tableinfo.second + " WHERE time_stamp > " + timestamp);
        if(!allp.empty()){
            qDebug() << tableinfo.first << allp.size();
            for(unsigned int i = 0;i<allp.size();i++){
                sql_item item = allp[i];

                // check uuid
                QString uuid = item.value("uuid").toString();

                QString basedonUuid = "";

                // uuid is based on combination of fields in some tables
                if (tableinfo.first == "school_schedule"){
                    basedonUuid = item.value("date").toString() + item.value("number").toString();
                }else if(tableinfo.first == "school"){
                    basedonUuid = item.value("school_schedule_id").toString() + item.value("classnumer").toString() + item.value("date").toString();
                }else if(tableinfo.first == "studies"){
                    basedonUuid = item.value("study_number").toString();
                }else if(tableinfo.first == "school_break"){
                    basedonUuid = item.value("date").toString() + item.value("classnumber").toString();
                }

                if (basedonUuid != ""){
                    uuid = QUuid::createUuidV5(QUuid(),tableinfo.first + basedonUuid).toString().remove("{").remove("}");
                }else if (item.value("uuid").toString() == ""){
                    uuid = QUuid::createUuid().toString().remove("{").remove("}");
                }

                if (uuid != item.value("uuid").toString()){
                    // insert or update uuid
                    sql_item s;
                    s.insert("uuid",uuid);
                    sql->updateSql(tableinfo.first,"id",item.value("id").toString(),&s);
                    item.insert("uuid",uuid);
                }

                item.insert("tablename",tableinfo.first);

                if(tableinfo.first == "school"){
                    qDebug() << "school_schedule_id " << item.value("school_schedule_id").toString();
                    qDebug() << "student_id " << item.value("student_id").toString();
                }
                // do not save time_stamp or id
                item.remove("time_stamp");
                item.remove("id");
                localchangeset.insert(item.value("uuid").toString(),item);
            }
        }
    }

    return localchangeset;
}

bool sync_cloud::getRemoteChangeSets()
{
    mRemoteChangeSet1.clear();
    mRemoteChangeSet2.clear();

    QJsonArray a = getChangesFromCloud();

    m_changeid = 0;

    if (!a.isEmpty()){
        foreach(QJsonValue v, a){
            QJsonObject o = v.toObject();
            qDebug() << "yksi change set" << o.value("change_id").toString();

            QString change_id_str = o.value("change_id").toString();
            if (m_changeid == 0) m_changeid = QVariant(change_id_str).toInt();

            //qDebug() << o.value("value");

            // [{"table": "persons","data":[ {"uuid":"1"},{"uuid":"2"} ] },{}]

            QJsonArray dataArray = QJsonDocument::fromJson(o.value("value").toString().toUtf8()).array();
            foreach(QJsonValue dataValue, dataArray){
                QJsonObject dataObject = dataValue.toObject();
                //qDebug() << "debugjm" << dataObject;
                //qDebug() << "table" << dataObject.value("table").toString() << "rowid" << dataObject.value("row_id").toString();

                QJsonArray rowsArray = dataObject.value("rows").toArray();
                QString table = dataObject.value("table").toString();

                foreach(QJsonValue v, rowsArray){
                    QJsonObject o = v.toObject();

                    QString uuId = o.value("uuid").toString();

                    // do not add if already in remotechangeset
                    if (mRemoteChangeSet1.contains(uuId) || mRemoteChangeSet2.contains(uuId)) continue;

                    sql_item item;
                    item.insert("tablename", table);

                    foreach(QString key, o.keys()){
                        //qDebug() << key + ":" + o.value(key).toString();
                        item.insert(key, o.value(key).toString());
                    }

                    if (table == "school_schedule" || table == "persons"){
                        mRemoteChangeSet1.insert(uuId,item);
                    }else{
                        mRemoteChangeSet2.insert(uuId,item);
                    }
                }
            }
        }
    }

    //qDebug() << "remoteitems" << remoteChangeset.count();
    return true;
}

void sync_cloud::saveChangesToDatabase(QHash<QString, sql_item> changeset)
{
    //sql->startTransaction();
    QHashIterator<QString, sql_item> i(changeset);
    while (i.hasNext()) {
        i.next();
        sql_item item = i.value();
        QString uuId = i.key();
        QString tableName = item.value("tablename").toString();
        item.remove("tablename");

        qDebug() << "uusi insert ???";
        sql->execSql(QString("INSERT INTO %1 (id,uuid) SELECT (SELECT MAX(id)+1 FROM %1),"
                             "'%2' WHERE NOT EXISTS (SELECT 1 FROM %1 WHERE uuid = '%2')").
                     arg(tableName,uuId));
        qDebug() << "update";

        // id mapping from uuid to internal id value
        if (tableName == "school"){
            item.insert("student_id",idMapping("persons",item.value("student_id").toString()));
            item.insert("assistant_id",idMapping("persons",item.value("assistant_id").toString()));
            item.insert("volunteer_id",idMapping("persons",item.value("volunteer_id").toString()));
            item.insert("school_schedule_id",idMapping("school_schedule",item.value("school_schedule_id").toString()));
        }else if (tableName == "student_studies"){
            item.insert("student_id",idMapping("persons",item.value("student_id").toString()));
        }else if (tableName == "unavailables"){
            item.insert("person_id",idMapping("persons", item.value("person_id").toString()));
        }

        for (sql_item::const_iterator i = item.constBegin(); i != item.constEnd(); ++i) {
            qDebug() << "d" << i.key() << i.value();
        }
        item.remove("time_stamp");
        item.remove("id");
        item.remove("uuid");
        sql->updateSql(tableName,"uuid",uuId,&item);
    }
    //sql->commitTransaction();
}

QString sync_cloud::idMapping(QString fromTable, QString uuid)
{
    if (uuid == "") return "-1";
    QString value = sql->selectSql(
                QString("SELECT IFNULL((SELECT id FROM %1 WHERE uuid = '%2'),-1) as id").arg(fromTable,uuid))[0].value("id").toString();

    return value;
}
