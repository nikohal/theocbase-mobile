import QtQuick 2.0
import QtQuick.Controls 1.1
import QtQuick.Controls.Styles 1.1
import QtQuick.Layouts 1.1
import net.theocbase.mobile 1.0

Rectangle {
    id: rectangle1
    implicitHeight: 600
    implicitWidth: 300

    property int detailid: -1

    function loadMe(id){
        console.log("load me" + id)
    }

    property SchoolDetail detobj

    ToolBar {
        id: topToolbar
        height: 50
        z: 2
        RowLayout {
            anchors.fill: parent
            ToolButton {
                Layout.fillWidth: true
                id: button1
                text: qsTr("Paluu")
                iconSource: "qrc:///icons/appbar.navigate.previous.png"
                onClicked: {
                    toolbar.height = 50
                    navbar.height = 50
                    // hide keyboard
                    Qt.inputMethod.hide()
                    // return to previous page
                    stackView.pop()
                }
                style: ToolButtonStyle{}
            }

            Label {
                Layout.fillWidth: true;
                text: "Detail Page"
            }
        }
    }

    Flickable {
        id: flickable1
        anchors.topMargin: 60
        anchors.bottomMargin: 10
        anchors.fill: parent
        contentHeight: layout.height
        flickableDirection: Flickable.VerticalFlick

        onFocusChanged: {
            console.log("flickable focus changed")
        }

        Column {
            id: layout
            spacing: 10
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.margins: 10

            Label {
                anchors.topMargin: 10
                id: label4
                text: qsTr("Student:")
                anchors.left: parent.left
                font.bold: true
            }

            Label {
                id: labelStudent
                text: detobj.student
                anchors.left: parent.left
                anchors.right: parent.right
            }

            Label {
                id: label2
                text: qsTr("Theme:")
                font.bold: true
                anchors.left: parent.left
            }

            Label {
                id: labelTheme
                text: detobj.theme
                wrapMode: Text.WordWrap
                anchors.right: parent.right
                anchors.left: parent.left
            }

            Label {
                id: label1
                text: qsTr("Source:")
                font.bold: true
                anchors.left: parent.left
            }

            Label {
                id: labelSource
                text: detobj.source
                wrapMode: Text.WordWrap
                anchors.right: parent.right
                anchors.left: parent.left
            }

            Label {
                id: labelStudy
                text: qsTr("Study:")
                anchors.left: parent.left
                font.bold: true
            }

            PickerPage {
                id: picker
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                model: detobj.studies
            }

            Label {
                id: labelNotes
                text: qsTr("Notes:")
                anchors.left: parent.left
                font.bold: true
            }

            TextArea {
                id: textArea1
                height: 280
                anchors.left: parent.left
                anchors.right: parent.right
                z: 1
            }
        }
        MouseArea {
            anchors.fill: parent
            z: -1
            onClicked: {
                if (Qt.inputMethod.visible) Qt.inputMethod.hide()
            }
        }

    }
}
