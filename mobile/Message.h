#ifndef MESSAGE_H
#define MESSAGE_H

#include <QObject>
#include <QDebug>

class Message : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString author READ author WRITE setAuthor NOTIFY authorChanged)
public:
    Message(){}
    ~Message(){}
    void setAuthor(const QString &a) {
        if (a != m_author) {
            m_author = a;
            emit authorChanged();
        }
    }
    QString author() const {
        return m_author;
    }

public slots:
    void saveValue(QString value){
        qDebug() << "This comes from C++ class:" << value;
    }

private:
    QString m_author;
signals:
    void authorChanged();
};

#endif // MESSAGE_H
