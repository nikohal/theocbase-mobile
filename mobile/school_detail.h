#ifndef SCHOOL_DETAIL_H
#define SCHOOL_DETAIL_H

#include <QObject>
#include "../school.h"

class school_detail : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString theme READ theme WRITE setTheme NOTIFY themeChanged)
    Q_PROPERTY(QString source READ source WRITE setSource NOTIFY sourceChanged)
    Q_PROPERTY(QString student READ studentName NOTIFY studentnameChanged)
    Q_PROPERTY(QVariant studies READ studies NOTIFY studiesChanged)

public:
    school_detail();

    void load(int id);

    QString theme();
    void setTheme(QString theme);

    QString source();
    void setSource(QString source);

    QString studentName();
    void setStudentName(QString name);

    QVariant studies();
    //Q_INVOKABLE void listUpdated(QVariant list);
signals:
    void themeChanged();
    void sourceChanged();
    void studentnameChanged();
    void studiesChanged();

private:
    QString mTheme;
    QString mSource;
    QString mStudentName;
};

#endif // SCHOOL_DETAIL_H
