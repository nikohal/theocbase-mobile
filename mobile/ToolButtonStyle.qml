import QtQuick 2.1
import QtQuick.Controls 1.1
import QtQuick.Controls.Styles 1.1

ButtonStyle {
    readonly property ToolButton control: __control
    property Component panel: Item {
        id: styleitem

        //TODO: Maybe we want to add a descriptive text at the bottom of the icon?
        implicitWidth: 40
        implicitHeight: 40

        opacity: control.pressed ? 0.5 : 1

        Text {
            id: label
            visible: icon.status != Image.Ready
            anchors.centerIn: parent
            text: control.text
        }

        Image {
            id: icon
            anchors.fill: parent
            fillMode: Image.PreserveAspectFit
            anchors.margins: 0
            source: control.iconSource
            z: 1

        }
        Rectangle {
            anchors.fill: parent
            //anchors.margins: -10
            color: "white"
            //radius: 2
            visible: control.checked
            opacity: 0.1
            z: -1
        }
    }
}
