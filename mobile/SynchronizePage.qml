import QtQuick 2.2
import QtQuick.Controls 1.2
import QtQuick.Controls.Styles 1.1

Rectangle {
    id: busypage
    width: 100
    height: 100
    color: "#ffffff"
    radius: 20
    visible: false

    function runSync()
    {
        busypage.visible = true
        text1.text = "Synchronizing..."
        ccloud.synchronize();
        console.log("sync ready");
        text1.text = "Ready"
        busy.running = false;
        busypage.visible = false;
    }

    BusyIndicator {
        id: busy

        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter

        anchors.centerIn: parent.Center

        running: true
        style: BusyIndicatorStyle {
            Rectangle {
                width: 50
                height: 50
                color: "lightsteelblue"

                RotationAnimator on rotation {
                    from: 0;
                    to: 360;
                    duration: 5000
                }
            }
        }
    }

    Text {
        id: text1
        x: 71
        y: 29
        text: qsTr("Syncronizing...")
        anchors.top: parent.top
        anchors.topMargin: 5
        anchors.horizontalCenter: parent.horizontalCenter
        font.pixelSize: 12
    }
}
