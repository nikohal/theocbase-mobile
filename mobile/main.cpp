#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QQuickItem>
#include <QString>
#include <QObject>
#include <QDebug>
#include <QObject>
#include "schoolview.h"
#include "Message.h"
#include "../sql_class.h"
#include "sync_cloud.h"
#include "publishers_modelview.h"
#include "cloud_controller.h"
#include "messagebox.h"

QString tempdatabasepath;
bool transactionStarted;

void initDatabase()
{
    // database from resource file
    QString databasepath = ":/database/theocbase.sqlite";
    QString localdatabasedir = QDir::tempPath();
    QString localdatabasepath = localdatabasedir + "/theocbase.sqlite";
    if(!QFile::exists(localdatabasepath)){
        // copy database to user's local
        QDir d;
        if(!d.exists(localdatabasedir)) d.mkdir(localdatabasedir);
        QFile::copy(databasepath,localdatabasepath);
        QFile::setPermissions(localdatabasepath,QFile::ReadOwner | QFile::WriteOwner |
                              QFile::ReadUser | QFile::WriteUser | QFile::ReadGroup |
                              QFile::WriteOwner | QFile::ReadOther | QFile::WriteOther);
        qDebug() << "New database copied to " + localdatabasedir;
        //QMessageBox::information(0,"",QObject::tr("Database copied to ") + localdatabasedir);
    }else{
        qDebug() << "Use existing database!";
    }

    sql_class *sql = &Singleton<sql_class>::Instance();
    sql->databasepath=localdatabasepath;
    tempdatabasepath=localdatabasepath;

    sql->createConnection();
    sql->updateDatabase("2014.03.0");

    QString v = sql->getSetting("appver");
    qDebug() << "version read from db" << v;

    sql->saveSetting("iOS_test",QDateTime::currentDateTime().toString());
    qDebug() << "Current time save to database" << sql->getSetting("iOS_test");

    //sql_item s;
    //s.insert("date","2014-01-01");
    //s.insert("classnumber",sql->selectSql("SELECT id FROM persons WHERE lastname = 'Burman'")[0].value("id").toString());
    //qDebug() << "insert test" << sql->insertSql("school_break",&s,"id");

}

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    qDebug() << app.applicationDirPath();
    qDebug() << "temppath" << QDir::tempPath();
    qDebug() << "homepath" << QDir::homePath();

    initDatabase();

    //sync_cloud s;
    //qDebug() << "is sync available?" << s.cloudUpdateAvailable();
    //s.sync();
    //s.postData();

    qmlRegisterType<publishers_modelview>("net.theocbase.mobile", 1, 0, "Publishers");
    qmlRegisterType<schoolview>("net.theocbase.mobile", 1, 0, "School");
    qmlRegisterType<school_detail>("net.theocbase.mobile", 1, 0, "SchoolDetail");
    qmlRegisterType<MessageBox>("net.theocbase.mobile",1,0,"MsgBox");

    //qmlRegisterType<SchoolViewItem>("net.theocbase.msg", 1, 0, "SchoolViewItem");

    QQmlApplicationEngine engine;
    QQmlContext *ctxt = engine.rootContext();

    //schoolview *sv = new schoolview(ctxt);
    //ctxt->setContextProperty("sview",sv);

    cloud_controller cc;
    ctxt->setContextProperty("ccloud", &cc);

    //publishers_modelview pp;
    //pp.setContext(ctxt);
    //ctxt->setContextProperty("pmodel",&pp);
    QString test = QString("SELECT * FROM exceptions WHERE < '%1'").arg("2012-01-01");
//    Message msg;
//    msg.setAuthor("QML TESTI");
//    ctxt->setContextProperty("msg",&msg);
    engine.load(QUrl(QStringLiteral("qrc:///main.qml")));
    return app.exec();
}
