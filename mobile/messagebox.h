#ifndef MESSAGEBOX_H
#define MESSAGEBOX_H

#include <QObject>

class MessageBox : public QObject
{
    Q_OBJECT

public:
    MessageBox();
    Q_INVOKABLE void show(QString title, QString message);
    Q_INVOKABLE void showOKCancel(QString title, QString message);
signals:
    Q_INVOKABLE void buttonClicked(bool ok);
public slots:

private:
};

#endif // MESSAGEBOX_H
