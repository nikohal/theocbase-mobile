#ifndef CLOUD_CONTROLLER_H
#define CLOUD_CONTROLLER_H

#include <QObject>
#include <QDebug>
#include <QString>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QNetworkAccessManager>
#include <QCryptographicHash>
#include <QEventLoop>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QSettings>
#include <QTimer>
#include "sync_cloud.h"
#include "../sql_class.h"

class cloud_controller : public QObject
{
    Q_OBJECT

public:
    cloud_controller();

    Q_INVOKABLE bool login(QString username, QString password);
    Q_INVOKABLE bool logged();
    Q_INVOKABLE void logout();
    Q_INVOKABLE bool checkCloudUpdates();
    Q_INVOKABLE void synchronize();
private:
    QString mUserName;
    int mUserId;
public slots:

signals:
    void loggedChanged(bool ok);
};

#endif // CLOUD_CONTROLLER_H
