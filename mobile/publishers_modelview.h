#ifndef PUBLISHERS_MODELVIEW_H
#define PUBLISHERS_MODELVIEW_H

#include <QObject>
#include <QString>
#include <QList>
#include <QDebug>
#include "../cpersons.h"

class cpersons;

class publishers_modelview : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)

public:
    publishers_modelview(){

    }

    void setName(QString value)
    {
        mName = value;
        emit nameChanged();
    }

    QString name()
    {
        return mName;
    }

    Q_INVOKABLE QVariant getAllPublishersList()
    {
        cpersons cp;
        QList<person *> all = cp.getAllPersons(0);

        QList<QObject *> list;
        qDebug() << "pubishers" << all.count();
        foreach(person *p, all){
            publishers_modelview *item = new publishers_modelview();
            item->setName(p->fullname());
            list.append(item);
        }

        return QVariant::fromValue(list);
    }

public slots:

signals:
    void nameChanged();

private:
    QString mName;
};

#endif // PUBLISHERS_MODELVIEW_H
