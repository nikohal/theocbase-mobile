TEMPLATE = app

QT += qml quick widgets sql network

#QMAKE_FRAMEWORK_BUNDLE_NAME = TheocBaseM
TARGET = TheocBaseMobile


SOURCES += main.cpp \
    ../sql_class.cpp \
    ../cpersons.cpp \
    ../person.cpp \
    ../ccongregation.cpp \
    ../school.cpp \
    ../school_item.cpp \
    ../schoolstudy.cpp \
    ../school_setting.cpp \
    ../meeting_dayandtime.cpp \
    sync_cloud.cpp \
    cloud_controller.cpp \
    school_detail.cpp \
    messagebox.cpp

RESOURCES += qml.qrc \
    ../images.qrc \
    ../database.qrc \
    icons.qrc

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Default rules for deployment.
include(deployment.pri)

HEADERS += \
    ../sql_class.h \
    ../singleton.h \
    ../cpersons.h \
    ../person.h \
    ../ccongregation.h \
    ../school.h \
    ../school_item.h \
    ../schoolstudy.h \
    ../school_setting.h \
    ../meeting_dayandtime.h \
    Message.h \
    sync_cloud.h \
    publishers_modelview.h \
    cloud_controller.h \
    schoolview.h \
    school_detail.h \
    messagebox.h


ios {
BUNDLE_DATA.files += $$PWD/iOS_BundleData/Icon.png
BUNDLE_DATA.files += $$PWD/iOS_BundleData/Default.png
BUNDLE_DATA.files += $$PWD/iOS_BundleData/Default-568h@2x.png
#BUNDLE_DATA.files += iosicons/Icon~ipad.png
#BUNDLE_DATA.files += iosicons/Icon-76.png
#BUNDLE_DATA.files += iosicons/Icon-76@2x.png
#$$PWD/iOS_BundleData/Default@2x.png \
#$$PWD/iOS_BundleData/Default-568h@2x.png
QMAKE_BUNDLE_DATA += BUNDLE_DATA
QMAKE_INFO_PLIST = iOS_BundleData/iosInfo.plist

OBJECTIVE_SOURCES += \
    messagebox.mm
SOURCES -= \
    messagebox.cpp
}
