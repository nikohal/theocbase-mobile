#ifndef SCHOOLVIEW_H
#define SCHOOLVIEW_H

#include <QObject>
#include <QString>
#include <QQmlContext>
#include <QQmlEngine>
#include <QDebug>
#include <QDate>
#include <QQmlListReference>
#include <QAbstractListModel>
#include <QQuickItem>
#include <QDate>
#include "../school.h"
#include "school_detail.h"

class SchoolViewItem : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString numbername READ numberName NOTIFY numberNameChanged)
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(QString value READ value WRITE setValue NOTIFY valueChanged)
    Q_PROPERTY(int idNumber READ idNumber WRITE setIdNumber NOTIFY idNumberChanged)
public:

        SchoolViewItem(QString number = "", QString name = "", QString value = "", int id = -1)
        {
            mNumberName = number;
            mName = name;
            mValue = value;
            mId = id;
        }

        void setNumberName(QString no) { mNumberName = no; }
        QString numberName() { return mNumberName; }

        void setName(QString name) { mName = name; }
        QString name() { return mName; }

        void setValue(QString value) { mValue = value; }
        QString value() { return mValue; }

        void setIdNumber(int id) { mId = id; }
        int idNumber() { return mId; }
private:
        int mId;
        QString mNumberName;
        QString mName;
        QString mValue;
signals:
    void nameChanged();
    void numberNameChanged();
    void valueChanged();
    void idNumberChanged();
};

class schoolview : public QObject
{
    Q_OBJECT

public:
    schoolview(){
        mFirstDayOfWeek = QDate::currentDate().addDays((QDate::currentDate().dayOfWeek()-1)*-1);
    }

    Q_INVOKABLE QDate currentDate()
    {
        return mFirstDayOfWeek;
    }

    Q_INVOKABLE void initList()
    {
        mFirstDayOfWeek = QDate::currentDate().addDays((QDate::currentDate().dayOfWeek()-1)*-1);
        updateList();
        emit listUpdated(QVariant::fromValue(mSchoolList));
    }

    Q_INVOKABLE void nextWeek()
    {
        mFirstDayOfWeek = mFirstDayOfWeek.addDays(7);
        updateList();
        emit listUpdated(QVariant::fromValue(mSchoolList));
    }

    Q_INVOKABLE void prevWeek()
    {
        mFirstDayOfWeek = mFirstDayOfWeek.addDays(-7);
        updateList();
        emit listUpdated(QVariant::fromValue(mSchoolList));
    }

    Q_INVOKABLE bool test(QObject *obj){

        qDebug() << obj;
        QQuickItem *item = qobject_cast<QQuickItem*>(obj);

        QQmlEngine *engine = qmlEngine(item);
        QQmlContext *context = engine->rootContext();
        qDebug() << context;

        QQmlListReference ref = obj->property("model").value<QQmlListReference>();
        //qDebug() << ref <<

        QList<QObject *> dList;
        dList.append(new SchoolViewItem("1","name","value",1));
        context->setContextProperty("schoollistmodel", QVariant::fromValue(dList));
//        QQmlListReference ref = obj->property("model").value<QQmlListReference>();

        return true;
    }

    Q_INVOKABLE school_detail *getAssignment(int index)
    {
        school_detail *det = new school_detail();
        school s_class;
        if (mSchoolList.size() > index){
            school_item item = s_class.getProgram(mFirstDayOfWeek).at(index);
            det->setTheme(item.getTheme());
            det->setSource(item.getSource());
            det->setStudentName(item.getStudent() ? item.getStudent()->fullname() : "");
            qDebug() << "getassignment";
        }
        return det;
    }

signals:
    Q_INVOKABLE void listUpdated(QVariant list);
private:
    QDate mFirstDayOfWeek;
    school s_class;
    QList<QObject *> mSchoolList;

    void updateList()
    {
        mSchoolList.clear();
        std::vector<school_item>list = s_class.getProgram(mFirstDayOfWeek);
        for(unsigned int i = 0;i<list.size();i++){
            school_item s = list[i];
            mSchoolList.append(new SchoolViewItem(s.getNumberName(),s.getTheme(),
                                                  s.getStudent() ? s.getStudent()->fullname() : "",
                                                  QVariant(s.id).toInt()));
        }
    }
};

#endif // SCHOOLVIEW_H
