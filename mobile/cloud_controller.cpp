#include "cloud_controller.h"

cloud_controller::cloud_controller()
{
    mUserName = "";
    QSettings settings;
    mUserId = settings.value("cloud/id",-1).toInt();
}

bool cloud_controller::login(QString username, QString password)
{
    mUserName = username;
    qDebug() << username << password;

    QNetworkAccessManager *m_manager = new QNetworkAccessManager();
    QString md5psw = QCryptographicHash::hash( password.toLatin1(),QCryptographicHash::Md5).toHex();

    QString urlstring = QString(
                "http://lab.theocbase.net/api.php?login=login&username=%1&psw=%2").arg(
                username, md5psw);
    QNetworkReply *reply = m_manager->get(
                QNetworkRequest( QUrl( urlstring )));

    QEventLoop loop;
    connect(reply,SIGNAL(finished()),&loop,SLOT(quit()));
    loop.exec();

    QVariant statusCode = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute);
    qDebug() << statusCode;
    int returnid = -1;
    if (statusCode.toInt() == 200) {
        //QString returnvalue = reply->readAll();
        QByteArray b = reply->readAll();
        QJsonDocument d = QJsonDocument::fromJson(b);
        QJsonObject obj = d.array().first().toObject();
        if (!obj.isEmpty()){
            qDebug() << obj;
            QString idstr = (obj.value("id").isUndefined() ? "-1" : obj.value("id").toString());
            returnid = idstr.toInt();
        }
    }
    mUserId = returnid;
    qDebug() << mUserId;
    QSettings settings;
    settings.setValue("cloud/id",returnid);
    emit loggedChanged(logged());
    return (logged());
}

bool cloud_controller::logged()
{
    return (mUserId > -1);
}

void cloud_controller::logout()
{
    mUserId = -1;
    QSettings settings;
    settings.setValue("cloud/id",-1);

    // clear database
    sql_class *sql = &Singleton<sql_class>::Instance();
    sql->saveSetting("last_sync_id","0");
    sql->saveSetting("last_sync_time","-1");
    sql->execSql("DELETE FROM persons");
    sql->execSql("DELETE FROM school");
    sql->execSql("DELETE FROM school_schedule");
    sql->execSql("DELETE FROM studies");
    sql->execSql("DELETE FROM school_break");
    sql->execSql("DELETE FROM school_settings");
    sql->execSql("DELETE FROM exceptions");
    sql->execSql("DELETE FROM student_studies");
    sql->execSql("DELETE FROM unavailables");
    emit loggedChanged(false);
}

bool cloud_controller::checkCloudUpdates()
{
    sync_cloud s;
    return (s.cloudUpdateAvailable());
}

void cloud_controller::synchronize()
{
    sync_cloud s;
    s.sync();
}
