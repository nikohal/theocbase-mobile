import QtQuick 2.2
import QtQuick.Controls 1.1
import QtQuick.Controls.Styles 1.1
import QtQuick.Layouts 1.1
import net.theocbase.mobile 1.0

ApplicationWindow {   
    id: applicationWindow1
    visible: true
    width: 480
    height: 800
    title: "Test Project"

    property int activepage: 1

    MsgBox {
        id: msg
    }

    StackView {
        id: stackView
        width: parent.width
        height: parent.height - navbar.height
        anchors.top: parent.top
        anchors.topMargin: 0

        z: 0

        onCurrentItemChanged: {
            if (stackView.currentItem.objectName === null ||stackView.currentItem.objectName === ""){
                // hide navbar
                console.log("hide navbar")
            }else{
                // show navbar
                console.log("show navbar")
            }
        }

        //initialItem: MainPage{}

        Component.onCompleted: {
            if (!ccloud.logged()){
                stackView.push([{item: Qt.resolvedUrl("MainPage.qml"), properties: {objectName: "page1"}},{item: Qt.resolvedUrl("LoginPage.qml")}])
            }else{
                stackView.push({item: Qt.resolvedUrl("MainPage.qml"), properties: {objectName: "page1"}})
//                if (ccloud.checkCloudUpdates()){
//                    msg.show("","Cloud updates available");
//                }
            }
        }
    }

    ToolBar {
        id: navbar
        z: 2
        height: 50
        anchors.bottomMargin: 0
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.left: parent.left
        RowLayout {
            anchors.fill: parent

            ToolButton {
                id: todayButton
                text: "Today"
                iconSource: "qrc:///icons/appbar.home.png"

                Layout.fillWidth: true
                Layout.fillHeight: true
                checkable: true
                checked: true
                onClicked: {
                    activepage = 1
                    checked = true
                    publishersButton.checked = false
                    settingsButton.checked = false
                    console.log("today clicked");
                    stackView.push({item: Qt.resolvedUrl("MainPage.qml"), immediate: true, properties: {objectName: "page1"}})
                    //properties: {fgcolor: "red", bgcolor: "blue"}})
                }

                style: ToolButtonStyle {}
            }
            ToolButton {
                id: publishersButton
                text: "Publishers"
                Layout.fillWidth: true
                Layout.fillHeight: true
                checkable: true
                iconSource: "qrc:///icons/appbar.people.multiple.png"
                onClicked: {
                    activepage = 2
                    checked = true
                    todayButton.checked = false
                    settingsButton.checked = false
                    console.log("publishers clicked");
                    stackView.push({item: Qt.resolvedUrl("PublishersPage.qml") ,immediate: true, properties: {objectName: "page2"}})
                }
                style: ToolButtonStyle {}
            }
            ToolButton {
                id: settingsButton
                text: "Settings"
                iconSource: "qrc:///icons/appbar.settings.png"
                Layout.fillWidth: true
                Layout.fillHeight: true
                checkable: true
                onClicked: {
                    activepage = 3
                    checked = true
                    todayButton.checked = false
                    publishersButton.checked = false
                    console.log("settings clicked");
                    stackView.push({item: Qt.resolvedUrl("SettingsPage.qml"),immediate: true, properties:{objectName: "page3"}})
                }
                style: ToolButtonStyle {}
            }
        }
        style: ToolBarStyle {
            padding {
                left: 0
                right: 0
                top: 0
                bottom: 0
            }
            background: Rectangle {
                implicitWidth: 100
                implicitHeight: 50
                border.width: 0
                color: "#116bc6"

            }
        }
    }
}

